(function ($) {
    "use strict"; // Start of use strict


    /* ---------------------------------------------
     Scripts initialization
     --------------------------------------------- */

    $(window).load(function () {

        // Page loader

        $("body").imagesLoaded(function () {
            $(".page-loader div").fadeOut();
            $(".page-loader").delay(200).fadeOut("slow");
        });

    });

    // Gallery thumbinail
    $(document).ready(function () {
        var groups = {};
        $('.galleryItem').each(function () {
            var id = parseInt($(this).attr('data-group'), 10);

            if (!groups[id]) {
                groups[id] = [];
            }

            groups[id].push(this);
        });


        $.each(groups, function () {

            $(this).magnificPopup({
                type:'image',
                closeOnContentClick:true,
                closeBtnInside:false,
                preloader:true,
                gallery:{enabled:true}
            })

        });
    });

    // Query.dotdotdot
    $(function () {
        $('.limited-item').dotdotdot();
    });
// bxSlider
    $(document).ready(function () {
        $('.bxslider').bxSlider();
    });

})(jQuery); // End of use strict

